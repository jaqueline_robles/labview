<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EntradaEstudents;

class adminController extends Controller
{
    public function indexAdmin() {

        $modo=EntradaEstudents::find(1)->activar;

    return view ('admin')->with(compact('modo'));
}




public function openRegister(Request $request){

    $open = EntradaEstudents::find(1);
    $open->activar= $request->activar;
    $open->save();
   
   return redirect ('admin');
 

}

public function closeRegister(Request $request){


    $close = EntradaEstudents::find(1);
    $close->activar= $request->activar;
    $close->save();

    return redirect ('admin');
}





}
