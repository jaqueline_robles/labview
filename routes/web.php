<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('index', 'indexController@index')->name('index');
Route::get('admin', 'adminController@indexAdmin')->name('admin');
Route::get('login', 'loginController@indexLogin')->name('login');
Route::post('request/acept', 'indexController@acceptRequest')->name('admin');
Route::post('openRegister', 'adminController@openRegister')->name('openRegister');
Route::post('closeRegister', 'adminController@closeRegister')->name('closeRegister');