<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>LabVIEW</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <!-- styles -->
  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="assets/css/prettyPhoto.css" rel="stylesheet" />
  <link href="assets/css/animate.css" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic|Roboto+Condensed:400,300,700" rel="stylesheet" />

  <link href="assets/css/style.css" rel="stylesheet" />
  <link href="assets/color/default.css" rel="stylesheet" />

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="ico/favicon.ico" />


</head>

<body>
  <header>
    <!-- start top -->
    <div id="topnav" class="navbar navbar-fixed-top default">
      <div class="navbar-inner">
        <div class="container">
          <div class="logo">
            <a href="index.html"></a>
          </div>
          <div class="navigation">
            <nav>
              <ul class="nav pull-right">
                <li class="current"><a href="#intro">Inicio</a></li>
           
            
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
          <!--/.nav-collapse -->
        </div>
      </div>
    </div>
    <!-- end top -->
  </header>
  <!-- section intro -->

  <!-- end intro -->

  <!-- section contact -->
  <section id="contact" class="section">
    

 

    <div class="container">
	 <center>
      <div class="row">
	  <center>
        
          <h4><i class="icon-ok"></i><strong>Solo Administrador puede iniciar sección</strong></h4>
          
          <!-- start contact form -->
         

            <ul class="contact-list">
              <li class="form-group">
                
                <li class="form-group">
                  <label>Correo Electronico <span>*</span></label>
                  <input type="email" class="form_input" name="email" id="email" placeholder="correo" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validation"></div>
                </li>
                <li class="form-group">
                  <label>Contraseña <span>*</span></label>
                  <input type="password" class="form_input" name="subject" id="subject" placeholder="contraseña" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </li>

           
                <li class="last">
                  <button class="btn btn-large btn-theme" type="submit" id="send">Iniciar</button>
                </li>
            </ul>
          </form>

          <!-- end contact form -->
        
       </center>
      </div>
	  </center>
    </div>
  </section>
  <!-- end section contact -->

  <footer>
    <div class="verybottom">
      <div class="container">
        <div class="row">
		  <br>
          <br>
		  		  <br>

		  <br>

		  <br>

		  <br>

        </div>
      </div>
    </div>
  </footer>
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-48 active"></i></a>

  <!-- Javascript Library Files -->

  <script src="assets/js/nagging-menu.js"></script>
  <script src="assets/js/jquery.nav.js"></script>
  <script src="assets/js/prettyPhoto/jquery.prettyPhoto.js"></script>
  <script src="assets/js/portfolio/jquery.quicksand.js"></script>
  <script src="assets/js/portfolio/setting.js"></script>
  <script src="assets/js/hover/jquery-hover-effect.js"></script>
  <script src="assets/js/jquery.scrollTo.min.js"></script>
  <script src="assets/js/animate.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Custom Javascript File -->
  <script src="assets/js/custom.js"></script>

</body>

</html>
