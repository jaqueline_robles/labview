<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Administrador</title>
  </head>
  <body>
  <div class="row">
     <div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	   <div class="row">
	    <div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
 <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Administrador</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Inicio </a>
      </li>
    </ul>
  </div>
</nav>
</div>
</div> 
 <div class="row">
  <div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
  <br>
  <br>
 
  </div>
  </div>
 


 <div class="row">
  <div  class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3"> 
  <center>
  <div class="card" style="width: 18rem;">
  <img src="assets/img/bg/profile.jpg" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">George Govan</h5>
    <p class="card-text">GeorgeTGovan@gustr.com</p>
     <p class="card-text">Profesor de LabVIEW</p>
  </div>
</div>

  </center>
  </div>
  
 <div  class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-xl-7">
<ul class="nav nav-tabs">
  <li class="nav-item" >
    <a class="nav-link active" style="background-color : #EFB810" href="#">Registro</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Solicitudes</a>
  </li>
 

</ul>

<div class="row">
 <div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
<br>
<br>
<div class="card">
    <h5 class="card-header" style="background-color : #007849"> El estado actual del registro es</h5>
    <div class="card-body">
      @if (($modo)==1)
      <h5 class="card-title" style="color:#007849"> activo </h5>
      @else 
      <h5 class="card-title" style="color: red"> Desactivado </h5>
      @endif
     
      
    </div>
  </div>
  <br>
  <br>
  <br>






<div class="card">
  <h5 class="card-header" style="background-color : #EFB810">Activar registro</h5>
  <div class="card-body">
    <h5 class="card-title">El registro será visible para todos</h5>
    <p class="card-text">En caso de no estar de acuerdo no presionar el boton.</p>
    <form id="serviceManual" action="{{url('openRegister')}}" method="POST"
    enctype="multipart/form-data">
    @csrf
    <input type="text" id="activar" value="1" name="activar" class="form_input" placeholder="Nombre" data-rule="minlen:4" data-msg="Please enter at least 4 chars" hidden />
    
    <button  id="botonActivar" type="submit" class="btn btn-dark">Aceptar</button>
    </form>
  </div>
</div>

<br>
<div class="card">
  <h5 class="card-header" style="background-color : #EFB810">Desactivar registro</h5>
  <div class="card-body">
    <h5 class="card-title">El registro no será visible </h5>
    <p class="card-text">En caso de no estar de acuerdo no presionar el boton.</p>
    <form id="serviceManual" action="{{url('closeRegister')}}" method="POST"
    enctype="multipart/form-data">
    @csrf
    <input type="text" id="activar" value="0" name="activar" class="form_input"  placeholder="Nombre" data-rule="minlen:4" data-msg="Please enter at least 4 chars" hidden />
    
    <button  type="submit" id="botonActivar2" class="btn btn-dark"  >Aceptar</button>
    </form>
  </div>
</div>

</div>
</div>


</div>
  <div  class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"> </div>
  </div>






</div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
 
 
 

  </body>
</html>